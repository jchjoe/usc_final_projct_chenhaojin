import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './comfolder/register/register.component';
import { BrowseComponent } from './comfolder/browse/browse.component';
import { OrderComponent } from './comfolder/order/order.component';
import {FormsModule} from "@angular/forms";
import {AuthService} from "./comfolder/shared/services/auth.service";
import {AppGuard} from "./app.guard";
import {LoginComponent} from "./comfolder/login/login.component";
import { HttpClientModule } from '@angular/common/http';
import { ShopingcartComponent } from './comfolder/shopingcart/shopingcart.component';
import { AdmproductComponent } from './comfolder/admproduct/admproduct.component';
import { AdmorderComponent } from './comfolder/admorder/admorder.component';
import { AddproductComponent } from './comfolder/addproduct/addproduct.component';
import { LogoutComponent } from './comfolder/logout/logout.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    BrowseComponent,
    OrderComponent,
    ShopingcartComponent,
    AdmproductComponent,
    AdmorderComponent,
    AddproductComponent,
    LogoutComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
      HttpClientModule
    ],
  providers: [AuthService, AppGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import {AuthService} from "../shared/services/auth.service";
import {ProductsrvService} from "../shared/services/productsrv.service";
import {Product} from "../shared/Product";
import {Cartitem} from "../shared/services/Cartitem";

@Component({
  selector: 'app-browse',
  templateUrl: './browse.component.html',
  styleUrls: ['./browse.component.css']
})
export class BrowseComponent implements OnInit {
  //@ts-ignore
  productl : Product[];
  //@ts-ignore
  product : Product = new Product();
  //@ts-ignore
  cartitem : Cartitem = new Cartitem();
  //@ts-ignore
  cartitem;


  constructor(private productsrvService: ProductsrvService, private authService : AuthService) { }

  ngOnInit(): void {
    this.productsrvService.findAll().subscribe(data => {
      this.productl = data;
    });
    this.authService.isAd().subscribe(res =>{
    })
  }

  public addToCart(id: number, count: number) {
    this.product.id = id;
    this.cartitem.product = this.product;
    this.cartitem.itcount = count;
    alert("Added to Cart");
    this.productsrvService.addToCart(this.cartitem, this.authService.username)
      .subscribe((res) => {

      });
  }





}

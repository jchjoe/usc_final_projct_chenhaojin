import { Component, OnInit } from '@angular/core';
import {AuthService} from "../shared/services/auth.service";
import {OrderserviceService} from "../shared/orders/orderservice.service";
import {Order} from "../shared/Order";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  order : Order[] = [];
  Object = Object;
  pricelst : number[] = [];
  constructor(private authService : AuthService, private orderserviceService : OrderserviceService) { }

  ngOnInit(): void {
    this.orderserviceService.findOrder(this.authService.username).subscribe(data => {
      this.order = data;

      for (let ord of this.order) {
        let price = 0;
        for (let itm of ord.itemlist) {
          price = price + itm.product.price * itm.itcount;
        }
        // @ts-ignore
        this.pricelst.push(price);
        price = 0;
      }

    });

  }

}

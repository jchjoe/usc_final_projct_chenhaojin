import { Component, OnInit } from '@angular/core';
import {Order} from "../shared/Order";
import {AuthService} from "../shared/services/auth.service";
import {OrderserviceService} from "../shared/orders/orderservice.service";

@Component({
  selector: 'app-admorder',
  templateUrl: './admorder.component.html',
  styleUrls: ['./admorder.component.css']
})
export class AdmorderComponent implements OnInit {

  order : Order[] = [];
  Object = Object;
  // @ts-ignore
  orderd : Order = new Order();
  constructor(private authService : AuthService, private orderserviceService : OrderserviceService) { }

  ngOnInit(): void {
    this.orderserviceService.findAllOrder().subscribe(data => {
      this.order = data;
    });
  }

  deleteOrder(id : number) {
    this.orderd.id = id;
    this.orderd.status = "Active";
    this.orderserviceService.deleteOrder(id).subscribe((res) => {
      this.ngOnInit();
    });

  }


}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmorderComponent } from './admorder.component';

describe('AdmorderComponent', () => {
  let component: AdmorderComponent;
  let fixture: ComponentFixture<AdmorderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdmorderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdmorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

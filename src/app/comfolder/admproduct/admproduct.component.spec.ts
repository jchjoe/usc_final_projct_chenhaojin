import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmproductComponent } from './admproduct.component';

describe('AdmproductComponent', () => {
  let component: AdmproductComponent;
  let fixture: ComponentFixture<AdmproductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdmproductComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdmproductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


});

import { Component, OnInit } from '@angular/core';
import {Product} from "../shared/Product";
import {Cartitem} from "../shared/services/Cartitem";
import {ProductsrvService} from "../shared/services/productsrv.service";
import {AuthService} from "../shared/services/auth.service";

@Component({
  selector: 'app-admproduct',
  templateUrl: './admproduct.component.html',
  styleUrls: ['./admproduct.component.css']
})
export class AdmproductComponent implements OnInit {
//@ts-ignore
  productl : Product[];
  //@ts-ignore
  product : Product;


  constructor(private productsrvService: ProductsrvService, private authService : AuthService) { }

  ngOnInit(): void {
    this.productsrvService.findAll().subscribe(data => {
      this.productl = data;
    });
  }

  editPro(id : number, productname : string, description : string, picrul : string, price : number) {
    // @ts-ignore
    this.product = new Product();
    this.product.id = id;
    this.product.description = description;
    this.product.picurl = picrul;
    this.product.price = price;
    this.productsrvService.editProduct(this.product).subscribe((res) => {
      this.ngOnInit();
    });
  }

  deleteProduct(id : number) {
    // @ts-ignore
    this.product = new Product();
    this.product.id = id;
    this.product.isshow = "notshow";
    this.productsrvService.editProduct(this.product).subscribe((res) => {
    });
  }


}

import { Component, OnInit } from '@angular/core';
import {ProductsrvService} from "../shared/services/productsrv.service";

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css']
})
export class AddproductComponent implements OnInit {

  constructor(private productsrvService : ProductsrvService) { }

  ngOnInit(): void {
  }

  //@ts-ignore
  addProduct(product){
    this.productsrvService.addProduct(product)
      .subscribe((res) => {
      });
  }

}

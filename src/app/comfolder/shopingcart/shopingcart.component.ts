import {Component, OnInit} from '@angular/core';
import {Cartitem} from "../shared/services/Cartitem";
import {AuthService} from "../shared/services/auth.service";
import {CartserviceService} from "../shared/carts/cartservice.service";
import {Order} from "../shared/Order";
import {Orderitem} from "../shared/Orderitem";
import {OrderserviceService} from "../shared/orders/orderservice.service";
import {Product} from "../shared/Product";

@Component({
  selector: 'app-shopingcart',
  templateUrl: './shopingcart.component.html',
  styleUrls: ['./shopingcart.component.css']
})
export class ShopingcartComponent implements OnInit {
  //@ts-ignore
  cartitemsl : Cartitem[];
  //@ts-ignore
  cartitem : Cartitem = new Cartitem();
  //@ts-ignore
  order : Order = new Order();

  orderitems : Orderitem[] = [];

  private orderitem: any;

  totalAmount : number = 0;



  constructor(private cartserviceService: CartserviceService, private authService : AuthService, private orderserviceService : OrderserviceService) { }

  ngOnInit(): void {
    this.cartserviceService.findAll(this.authService.username).subscribe(data => {
      this.cartitemsl = data;
      this.totalAmount = 0;
      for(let a of data){
        this.totalAmount += a.itcount * a.product.price;
      }
    });
  }



  public deleteItem(id : number) {
    this.cartserviceService.deleteItem(id).subscribe((res) => {
      this.ngOnInit();
    });
  }

  public changeCount(id : number, count: number) {
    this.cartitem.id = id;
    this.cartitem.itcount = count;
    this.cartserviceService.editItem(this.cartitem, this.authService.username).subscribe((res) => {
      this.ngOnInit();
    });
  }

  public placeOrder() {
    this.orderitems = [];
    // @ts-ignore
    for(let cartiem of this.cartitemsl) {
      //@ts-ignore
      this.orderitem = new Orderitem();
      // @ts-ignore
      this.orderitem.product = new Product();
      this.orderitem.product.id = cartiem.product.id;
      this.orderitem.itcount = cartiem.itcount;
      this.orderitems.push(this.orderitem);
    }
    this.order.itemlist = this.orderitems;
    this.order.status = "Active";
    this.orderserviceService.placeOrder(this.order, this.authService.username)
      .subscribe((res) => {
    });
    for(let itm of this.cartitemsl) {
      this.cartserviceService.deleteItem(itm.id)
        .subscribe((res) =>{

        });

    }

    alert("Thanks for your order");


  }


}

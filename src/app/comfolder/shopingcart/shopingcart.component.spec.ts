import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopingcartComponent } from './shopingcart.component';
import {CartserviceService} from "../shared/carts/cartservice.service";
import {HttpClient, HttpClientModule, HttpHandler} from "@angular/common/http";
import {of} from "rxjs";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {Cartitem} from "../shared/services/Cartitem";

describe('ShopingcartComponent', () => {
  let component: ShopingcartComponent;
  let fixture: ComponentFixture<ShopingcartComponent>;
  let service : CartserviceService;


  beforeEach(async () => {
    // @ts-ignore
    service = new CartserviceService();
    await TestBed.configureTestingModule({
      declarations: [ ShopingcartComponent ],
      providers: [
      HttpClient,
      HttpHandler,
        HttpClientModule
    ],
      imports :[
        HttpClientTestingModule
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShopingcartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("test cart total", () => {
    const cl = [
      {
        "id": 36,
        "itcount": 2,
        "product": {
          "productname": "peach",
          "price": "15",
          "description": "this is peach",
          "picurl": "assets/pic/pear.jpeg",
          "isshow": "show",
          "id": 253
        }
      },
      {
        "id": 37,
        "itcount": 4,
        "product": {
          "productname": "apple",
          "price": "5",
          "description": "this is apple",
          "picurl": "assets/pic/apple.jpeg",
          "isshow": "show",
          "id": 254
        }
      }
    ];
    let res;
    let totalAmount;

    // @ts-ignore
    spyOn(service, "findAll").and.returnValue(of(cl));
    service.findAll("testjoe").subscribe(data => {
      res = data;
      totalAmount = 0;
      for(let a of data){
        totalAmount += a.itcount * a.product.price;
      }
    });
    // @ts-ignore
    expect(totalAmount).toEqual(50);
    // @ts-ignore
    expect(res).toEqual(cl);


  });


});

import { Injectable } from '@angular/core';
import {AppConfig} from "../services/app.config";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {map, Observable} from "rxjs";
import {Product} from "../Product";
import {Cartitem} from "../services/Cartitem";

@Injectable({
  providedIn: 'root'
})
export class CartserviceService {
  private APT_URL = AppConfig.API_URL;

  constructor(private http: HttpClient, private router: Router) { }

  public findAll(username : String): Observable<Cartitem[]> {
    return this.http.get<Cartitem[]>(this.APT_URL + "/cartitem/" + username);
  }

  public deleteItem(id : number): Observable<any> {
    return this.http.delete(this.APT_URL + "/cartitem/" + id);
  }

  public editItem(cartitem : Cartitem, username : string) : Observable<any> {
    return this.http.put(this.APT_URL + "/cartitem/" + username, cartitem)
      .pipe(map(res => {
    }));
  }
}

export class Product {
  id : number;
  productname : string;
  description : string;
  price : number;
  picurl : string;
  isshow : string


  constructor(id: number, productname: string, description: string, price: number, picurl: string, isshow: string) {
    this.id = id;
    this.productname = productname;
    this.description = description;
    this.price = price;
    this.picurl = picurl;
    this.isshow = isshow;
  }
}

import { Injectable } from '@angular/core';
import {AppConfig} from "../services/app.config";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {map, Observable} from "rxjs";
import {Cartitem} from "../services/Cartitem";
import {Order} from "../Order";

@Injectable({
  providedIn: 'root'
})
export class OrderserviceService {
  private APT_URL = AppConfig.API_URL;

  constructor(private http: HttpClient, private router: Router) { }

  //admin get all order
  public findAllOrder() : Observable<Order[]>{
    return this.http.get<Order[]>(this.APT_URL + "/order/all");
  }

//  show user order
  public findOrder(username : string) : Observable<Order[]>{
    return this.http.get<Order[]>(this.APT_URL + "/order/" + username);
  }

//  place order
  public placeOrder(order : Order, username : string) : Observable<any> {
    // @ts-ignore
    return this.http.post(this.APT_URL + "/order/" + username, order)
      .pipe(map(res => {
        this.router.navigate(['/bi']);
        return res;
    }));
  }

  public deleteOrder(id : number) : Observable<any> {
    // @ts-ignore
    return this.http.delete(this.APT_URL + "/order/" + id)
      .pipe(map(res => {
        return res;
    }));
  }

}

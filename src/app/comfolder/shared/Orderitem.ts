import {Product} from "./Product";

export class Orderitem {
  id : number;
  itcount : number;
  // @ts-ignore
  product : Product;

  constructor(id: number, itcount: number, product: Product) {
    this.id = id;
    this.itcount = itcount;
    this.product = product;
  }


}

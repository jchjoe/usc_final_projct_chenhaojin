// @ts-nocheck

import { Injectable } from '@angular/core';
import {AppConfig} from "./app.config";
import {map, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {Product} from "../Product";
import {Cartitem} from "./Cartitem";

@Injectable({
  providedIn: 'root'
})
export class ProductsrvService {

  private APT_URL = AppConfig.API_URL;


  constructor(private http: HttpClient, private router: Router) { }

  public findAll(): Observable<Product[]> {
    return this.http.get<Product[]>(this.APT_URL + "/product");
  }

  //@ts-ignore
  addToCart(cartItem : Cartitem, username : string) : Observable<any> {
    return this.http.post(this.APT_URL + "/cartitem/" + username, cartItem)
      .pipe(map(res => {
        return res;
      }));
  }

  deleteProduct(productid : string) : Observable<any> {
    return this.http.delete(this.APT_URL + "/product" + productid)
      .pipe(map(res =>{
      }));
  }

  // @ts-ignore
  editProduct(product : Product) : Observable<any> {
    return this.http.put(this.APT_URL + "/product", product)
      .pipe(map(res =>{
        return res;
      }));
  }

  addProduct(product): Observable<any> {
    return this.http.post(this.APT_URL + '/product', product)
      .pipe(map(res => {
        console.log(res);
        if(res.success) {
          this.router.navigate(['/admp']);
        } else {
          alert("This product has existed.")
        }
      }));
  }

}

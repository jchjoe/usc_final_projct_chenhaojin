import { TestBed} from '@angular/core/testing';
import { of } from 'rxjs';
import { ProductsrvService } from './productsrv.service';
import {HttpClient, HttpHandler} from "@angular/common/http";
import {BrowseComponent} from "../../browse/browse.component";
import {HttpClientTestingModule} from "@angular/common/http/testing";

describe('ProductsrvService', () => {
  let productsrvService : ProductsrvService;
  // @ts-ignore
  let comp : BrowseComponent = new BrowseComponent();
  beforeEach(async () => {
    // @ts-ignore
    productsrvService = new ProductsrvService();
    await TestBed.configureTestingModule({
      declarations: [
        BrowseComponent,
        ProductsrvService
      ],
      providers: [
        HttpClient,
        HttpHandler
      ],
      imports :[
      HttpClientTestingModule
    ]
    }).compileComponents();

  });

  it("should be created", () => {
    expect(productsrvService).toBeTruthy();
  });

  it('test get all', () => {
    const pl = [
      {
        "productname": "peach",
        "price": "15",
        "description": "this is peach",
        "picurl": "assets/pic/pear.jpeg",
        "isshow": "show",
        "id": 253
      },
      {
        "productname": "apple",
        "price": "5",
        "description": "this is apple",
        "picurl": "assets/pic/apple.jpeg",
        "isshow": "show",
        "id": 254
      }
    ];
    let response;
    // @ts-ignore
    spyOn(productsrvService, "findAll").and.returnValue(of(pl));
    productsrvService.findAll().subscribe( res => {
      response = res;
    });
    // @ts-ignore
    expect(response).toEqual(pl);
  });

});

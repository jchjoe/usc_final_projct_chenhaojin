//@ts-nocheck
import { Injectable } from '@angular/core';
import {AppConfig} from "./app.config";
import {BehaviorSubject, map, Observable, Subject} from "rxjs";
import {Router} from "@angular/router";
import {HttpClient, HttpParams} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  username : string;

  private APT_URL = AppConfig.API_URL;

  isAdmin: Subject<boolean> = new BehaviorSubject<boolean>(false);

  loggedIn: Subject<boolean>  = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient, private router: Router) { }

  login(user) : Observable<any> {
    let params = new HttpParams();
    params = params.append("username", user.username);
    params = params.append("password", user.password);
    this.username = user.username;
    return this.http.post(this.APT_URL + "/login", params, {withCredentials: true})
      .pipe(map((res) => {
        this.loggedIn.next(res.success);
        if (this.loggedIn) {
          this.router.navigate(['/bi']);
        }
        return res;
      }));
  }

  checklogin(): Observable<any> {
    return this.http.get(this.APT_URL + "/checklogin", {withCredentials: true})
      .pipe(map((res) => {
        this.loggedIn.next(res.success);
        return res;
      }))
  }

  logout(): Observable<any> {
    //withCredentials: true 每次发请求都会把token加到header里
    return this.http.post(this.APT_URL + "/logout", {}, {withCredentials: true})
      .pipe(map(res => {
        this.loggedIn.next(false);
        this.router.navigate(['/login']);
        return res;
      }));
  }

  register(user): Observable<any> {
    return this.http.post(this.APT_URL + '/users', user)
      .pipe(map(res => {
        console.log(res);
        if(res.success) {
          this.router.navigate(['/login']);
        }
      }));
  }

  isAd() : Observable<any>{
    return this.http.get(this.APT_URL + '/users/test',{withCredentials: true}).pipe(map(res=>{
      this.isAdmin.next(res.success)
    }));
  }
}

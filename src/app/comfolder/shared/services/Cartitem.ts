import {Product} from "../Product";

export class Cartitem {
  id : number;
  itcount : number;
  product : Product;


  constructor(id: number, itcount: number, product: Product) {
    this.id = id;
    this.itcount = itcount;
    this.product = product;
  }
}




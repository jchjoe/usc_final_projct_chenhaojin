import {Orderitem} from "./Orderitem";

export class Order {
  id : number;
  status : string;
  user : any;
  itemlist : Orderitem[];


  constructor(id: number, status: string, user: any, itemlist: Orderitem[]) {
    this.id = id;
    this.status = status;
    this.user = user;
    this.itemlist = itemlist;
  }
}

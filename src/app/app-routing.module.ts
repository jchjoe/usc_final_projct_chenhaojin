import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowseComponent } from './comfolder/browse/browse.component';
import { OrderComponent } from './comfolder/order/order.component';
import { LoginComponent } from './comfolder/login/login.component';
import {RegisterComponent} from "./comfolder/register/register.component";
import {AppGuard} from "./app.guard";
import {ShopingcartComponent} from "./comfolder/shopingcart/shopingcart.component";
import {AdmproductComponent} from "./comfolder/admproduct/admproduct.component";
import {AdmorderComponent} from "./comfolder/admorder/admorder.component";
import {AddproductComponent} from "./comfolder/addproduct/addproduct.component";
import {LogoutComponent} from "./comfolder/logout/logout.component";



const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: '',
    canActivate: [AppGuard],
    children :[
      {
        path:'bi',
        component:BrowseComponent
      },
      {
        path:'oh',
        component:OrderComponent
      },
      {
        path:'sc',
        component:ShopingcartComponent
      },
      {
        path:'admp',
        component:AdmproductComponent
      },
      {
        path:'admo',
        component:AdmorderComponent
      },
      {
        path:'adp',
        component:AddproductComponent
      },
      {
        path: 'logout',
        component: LogoutComponent
      }
    ]
  },
  {
    path:'login',
    component:LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
